#include <iostream>

namespace yase {

void HelloWorld() {
  std::cout << "Hello World! Nothing here yet." << std::endl;
}

}  // namespace yase
