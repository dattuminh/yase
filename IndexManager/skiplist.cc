#include <random>
#include "skiplist.h"
#include <time.h>
#include <iostream>

namespace yase {

    SkipList::SkipList(uint32_t key_size) : key_size(key_size), height(1) {
        for (uint32_t i = 0; i < SkipListNode::kMaxLevels; ++i) {
            head.next[i] = &tail;
            tail.next[i] = nullptr;
            level_lock[i] = 0;
            pthread_rwlock_init(&rwlock[i], NULL);
        }
    }

    SkipList::~SkipList() {
        // Deallocate all the towers allocated in memory

        SkipListNode *current_node = head.next[SkipListNode::kMaxLevels-1];
        while (current_node && current_node != &tail) {
            SkipListNode *nextNode = nullptr;

            if (current_node->next[SkipListNode::kMaxLevels-1]) {
                nextNode = current_node->next[SkipListNode::kMaxLevels-1];
            }

            free(current_node);
            current_node = nextNode;
        }

        //destroy rwlock array
        for(uint32_t i=0; i<SkipListNode::kMaxLevels; ++i){
            pthread_rwlock_destroy(&rwlock[i]);
        }
    }

    int SkipList::CompareKey(char *k1, char *k2) {
        return memcmp(k1, k2, key_size);
    }

    // Lock read or write each level & print for debugging purpose
    void SkipList::Lock_readwrite(uint32_t lock_code, uint32_t lock_heights, uint32_t lock_key, uint32_t lock_rid) {
        for (uint32_t  i = 0; i < SkipListNode::kMaxLevels; i++ ){
            if (i <= lock_heights){
                //determine lock type by lock coe
                if (lock_code == 1) {
                    int k = pthread_rwlock_wrlock(&rwlock[i]);
                    if (k != 0) {
                        //  std::cout << "== ERROR write lock : " << " Code=" << k << " lock_code =" << lock_code << " i= " << i <<
                        //   " height_new_tower=" << lock_heights << std::endl;
                    } else {
                        level_lock[i]++;
                        //  std::cout << "== OK write lock : " << "k= " << lock_key << " rid= " << lock_rid << " lock_code =" << lock_code << " i= " << i <<
                        //     " height_new_tower=" << lock_heights << std::endl;
                    }
                } else {
                    int k = pthread_rwlock_rdlock(&rwlock[i]);
                    if (k!=0) {
                        //  std::cout << "== ERROR read lock : " << " Code=" << k << " lock_code =" << lock_code << " i= " << i <<
                        //     " height_new_tower=" << lock_heights << std::endl;
                    } else {
                        level_lock[i]++;
                        //  std::cout << "== OK read lock : " << "k= " << lock_key << " rid= " << lock_rid << " lock_code =" << lock_code << " i= " << i <<
                        //     " height_new_tower=" << lock_heights << std::endl;
                    }
                }
            }
            else {
                int k = pthread_rwlock_rdlock(&rwlock[i]);
                if (k!=0) {
                    //  std::cout << "== Error read lock : " << " Code=" << k << " lock_code =" << lock_code << " i= " << i <<
                    //     " height_new_tower=" << lock_heights << std::endl;
                } else {
                    level_lock[i]++;
                    //  std::cout << "== OK read lock : " << "k= " << lock_key << " rid= " << lock_rid << " lock_code =" << lock_code << " i= " << i <<
                    //     " height_new_tower=" << lock_heights << std::endl;
                }
            }
        }
    }

    // Unlock levels
    void SkipList::Unlock_readwrite() {
        for (uint32_t  i = 0; i < SkipListNode::kMaxLevels; i++ ){
            level_lock[i]--;
            pthread_rwlock_unlock(&rwlock[i]);
        }
    }

    SkipListNode *SkipList::NewNode(uint32_t levels, char *key, RID rid) {
        // 1. Use malloc to allocate the node, including SkipListNode itself and the
        //    key that follows (i.e., starts at SkipListNode.key).
        // 2. Use in-place new and the SkipListNode(uint32_t nlevels, RID rid)
        //    constructor to initialize the node
        // 3. Copy the key to the node's key area

        auto *node = (SkipListNode *) malloc(sizeof(SkipListNode) + key_size);
        new (node) SkipListNode(levels, rid);
        memcpy(node->key, key, key_size);
        return node;
    }

    SkipListNode *SkipList::Traverse(char *key, std::vector<SkipListNode*> *out_pred_nodes) {
        // Start from the head dummy tower to reach the specified key. If the key
        // exists, return the tower that represents the key, otherwise return nullptr
        // to indicate the key does not exist.
        //
        // Keep predecessor nodes (i.e., whenever the search path "turns down") in the
        // user-provided vector. Note that out_pred_nodes might be nullptr if the user
        // is Read() or Update for which the predecessor nodes won't be useful.

        if (out_pred_nodes) {
            for (uint32_t i = 0; i < SkipListNode::kMaxLevels; i++) {
                out_pred_nodes->push_back(&head);
            }
        }

        SkipListNode *current_node = &head;

        // Iterate the tower that is the closest to the key
        for (uint32_t i = SkipListNode::kMaxLevels - height; i < SkipListNode::kMaxLevels; i++) {

            // Keep iterating through all the nodes until you find the key or tower
            // if the next elements key is less than or equal to what we are trying to find
            // set the new head to that value
            while (current_node->next[i]) {
                int key_comparison = CompareKey(current_node->next[i]->key, key);

                if (out_pred_nodes && key_comparison < 0) {
                    for (uint32_t j = i; j < SkipListNode::kMaxLevels; j++) {
                        out_pred_nodes->at(j) = current_node->next[i];
                    }
                }

                if (key_comparison < 0) {
                    current_node = current_node->next[i];
                }

                else if (key_comparison == 0) {
                    return current_node->next[i];
                }

                else {
                    break;
                }
            }
        }

        // Return nullptr as key was not found
        return nullptr;
    }

    uint32_t SkipList::ChooseHeight() {
        srand(time(nullptr));
        return (rand() % SkipListNode::kMaxLevels);
    }

    bool SkipList::Insert(char *key, RID rid) {
        // Use the Traverse function to reach the insertion point:
        // 1. if Traverse returns a valid node, then the key already exists in the skip
        //    list and return false;
        // 2. otherwise continue to:
        //    (a) determine the height of the new tower
        //    (b) build up the tower from bottom up using the vector passed in to the
        //        Traverse function: it should contain all the predecessors at each
        //        level
        //    (c) return true/false to indicate a successful/failed insert

        // Get a height between 0 - 7
        uint32_t height_new_tower = ChooseHeight();
        uint32_t lock_code = 1;
        uint32_t lock_key = rid.value/2;
        uint32_t lock_rid = rid.value;

        Lock_readwrite(lock_code, height_new_tower, lock_key, lock_rid);

        std::vector<SkipListNode*> out_pred_nodes;
        SkipListNode* node = Traverse(key, &out_pred_nodes);

        if (node) {
            // Unlock found node
            Unlock_readwrite();
            return false;
        }

        // set the maximum height
        if (height < height_new_tower+1) {
            height = height_new_tower+1;
        }

        // Use kMaxLevels here as the largest kMaxLevel is the bottom level of the skiplist whereas the height variable
        // only knows the maximum height from the kMaxLevels variable to the tallest tower
        uint32_t current_list_level = SkipListNode::kMaxLevels - 1;
        uint32_t height_tower_stop = current_list_level - height_new_tower;

        SkipListNode* new_node = NewNode(height_new_tower, key, rid);

        // Move from bottom of the tower to the top
        while(current_list_level >= height_tower_stop) {
            new_node->next[current_list_level] = out_pred_nodes[current_list_level]->next[current_list_level];
            out_pred_nodes[current_list_level]->next[current_list_level] = new_node;

            if (current_list_level == 0) {
                break;
            }

            current_list_level--;
        }

        //lock free search for insertion
        RID SearchResult = SearchLockFree(key);
        bool node_exists = SearchResult.value == new_node->rid.value;
        //std::cout << "== unlock & insert success(1) : " << node_exists << " key=" << rid.value/2 << std::endl;

        // Unlock after insert to skiplist
        Unlock_readwrite();
        return node_exists;

    }

    RID SkipList::Search(char *key) {
        uint32_t lock_code = 0;
        uint32_t lock_key = 0;
        uint32_t lock_rid = 0;

        Lock_readwrite(lock_code, SkipListNode::kMaxLevels, lock_key, lock_rid);

        auto *node = Traverse(key);

        Unlock_readwrite();

        return node ? node->rid : RID();
    }

    RID SkipList::SearchLockFree(char *key) {
        auto *node = Traverse(key);
        return node ? node->rid : RID();
    }


    bool SkipList::Update(char *key, RID rid) {
        // Invoke the Traverse function to obtain the target tower and update its
        // payload RID. Returns true if the key is found and the RID is updated,
        // otherwise return false.
        uint32_t lock_code = 0;
        uint32_t lock_key = 0;
        uint32_t lock_rid = rid.value;

        Lock_readwrite(lock_code, SkipListNode::kMaxLevels, lock_key, lock_rid);

        auto *node = Traverse(key);

        if (node) {
            node->rid = rid;
            bool update_successful = node->rid.IsValid() && (node->rid.value == rid.value);
            Unlock_readwrite();
            return update_successful;
        }

        Unlock_readwrite();
        return false;
    }

    bool SkipList::Delete(char *key) {
        // Similar to Insert(), use the Traverse function to obtain the target node
        // that contains the provided key and the predecessor nodes, then unlink the
        // returned node at all levels.
        //
        // The unlinked tower should be freed.
        //
        // Return true if the operation succeeded, false if the key is not found.

        uint32_t lock_code = 1;
        uint32_t lock_key = 0;
        uint32_t lock_rid = 0;

        Lock_readwrite(lock_code, SkipListNode::kMaxLevels, lock_key, lock_rid);

        std::vector<SkipListNode*> previous_nodes;
        SkipListNode* node = Traverse(key, &previous_nodes);

        if (node) {

            // Need to subtract 1 as the nLevels go from 0-7 whereas the height calculation is from 1-8
            uint32_t height_node = SkipListNode::kMaxLevels - node->nlevels - 1;

            for (uint32_t i = height_node; i < SkipListNode::kMaxLevels; i++) {
                SkipListNode* current_node = previous_nodes[i];
                if (current_node->next[i]) {

                    current_node->next[i] = node->next[i];
                    node->next[i] = nullptr;
                }
            }

            bool successful_delete = !SearchLockFree(key).IsValid();

            Unlock_readwrite();
            return successful_delete;
        }

        Unlock_readwrite();
        return false;

    }

}  // namespace yase
