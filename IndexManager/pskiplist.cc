#include <random>
#include <time.h>
#include <iostream>
#include "../BufferManager/table.h"

namespace yase {

    std::random_device randomDevice;  //Will be used to obtain a seed for the random number engine
    std::mt19937 engine(randomDevice()); //Standard mersenne_twister_engine seeded with randomDevice()
    std::uniform_int_distribution<uint64_t > distribution(0, PSkipListNode::kMaxLevels-1);

    PSkipList::PSkipList(std::string name, uint32_t key_size) : key_size(key_size), height(1) {
        static const uint16_t FileID = 0;
        static const uint64_t kDataSize = 16 * yase::MB;
        static const uint32_t kRecordSize = sizeof(PSkipListNode) + key_size;

        table = new Table(std::move(name), FileID, kRecordSize, kDataSize);

        for (uint32_t i = 0; i < PSkipListNode::kMaxLevels; ++i) {
            head.next[i] = tail.rid;
        }

        RID head_RID = table->Insert((char *) &head);
        head.TableRID = head_RID;
        table->Update(head_RID, (char *) &head);
    }

    PSkipList::~PSkipList() {
        // Deallocate all the towers allocated in memory
    }

    int PSkipList::CompareKey(char *k1, char *k2) {
        return memcmp(k1, k2, key_size);
    }

    PSkipListNode *PSkipList::NewNode(uint32_t levels, char *key, RID rid) {
        // 1. Use malloc to allocate the node, including SkipListNode itself and the
        //    key that follows (i.e., starts at SkipListNode.key).
        // 2. Use in-place new and the SkipListNode(uint32_t nlevels, RID rid)
        //    constructor to initialize the node
        // 3. Copy the key to the node's key area

        auto *node = (PSkipListNode *) malloc(sizeof(PSkipListNode) + (key_size));
        new (node) PSkipListNode(levels, rid);
        memcpy(node->key, key, key_size);

        // Set all the next[i] values to nullptr
        for (uint32_t i = 0; i < PSkipListNode::kMaxLevels; i++) {
            node->next[i] = tail.rid;
        }

        RID new_node_table_rid = table->Insert((char *) node);
        node->TableRID = new_node_table_rid;
        table->Update(new_node_table_rid, (char *) node);

        return node;
    }

    PSkipListNode *PSkipList::Traverse(char *key, std::vector<RID> *out_pred_nodes) {
        // Start from the head dummy tower to reach the specified key. If the key
        // exists, return the tower that represents the key, otherwise return nullptr
        // to indicate the key does not exist.
        //
        // Keep predecessor nodes (i.e., whenever the search path "turns down") in the
        // user-provided vector. Note that out_pred_nodes might be nullptr if the user
        // is Read() or Update for which the predecessor nodes won't be useful.

        // If we want the previous RID, start by populating the vector with the head node
        if (out_pred_nodes) {
            for (uint32_t i = 0; i < PSkipListNode::kMaxLevels; i++) {
                out_pred_nodes->push_back(head.TableRID);
            }
        }

        // Create the pskiplist nodes needed for traversal
        PSkipListNode *current_node = &head;
        PSkipListNode *bufferedNode = nullptr;
        PSkipListNode *nextBufferedNode = nullptr;

        // Create the buffers for loading the data from the table into the created pskiplist nodes
        void* out_buf_curr = (PSkipListNode*) malloc(sizeof(PSkipListNode) + key_size);

        // Load the head node
        bool successful_read = table->Read(current_node->TableRID, out_buf_curr);
        if (!successful_read) {
            return nullptr;
        }

        // If the  Ahead node was read successfully, then continue
        bufferedNode = (PSkipListNode *)out_buf_curr;

        // Increment the height of the skip list starting from the height of the tallest tower down to the bottom level (lvl 7)
        for (uint32_t i = PSkipListNode::kMaxLevels - height; i < PSkipListNode::kMaxLevels; i++) {

            // While the next node in the next[] is a valid table rid
            while (bufferedNode->next[i].IsValid()) {

                // Get the rid of the next node
                RID nextNodeRID = bufferedNode->next[i];

                // Check to make sure that the next node is valid
                if (nextNodeRID.IsValid()) {
                    void* out_buf_next = (PSkipListNode*) malloc(sizeof(PSkipListNode) + key_size);

                    // Read in the next node from the table
                    successful_read = table->Read(nextNodeRID, out_buf_next);
                    if (!successful_read) {
                        free(out_buf_next);
                        break;
                    }

                    nextBufferedNode = (PSkipListNode *) out_buf_next;

                    // Is the current key value less than the key we are trying to find
                    int key_comparison = CompareKey(nextBufferedNode->key, key);
                    if (out_pred_nodes && key_comparison < 0) {

                        // Update all the values in the vector with RID's of nodes with keys < the search key
                        for (uint32_t j = i; j < PSkipListNode::kMaxLevels; j++) {
                            out_pred_nodes->at(j) = nextBufferedNode->TableRID;
                        }
                    }

                    // If the currently loaded next key is smaller than the key we are searching for, update the
                    // buffered node pointer to point to the next key
                    if (key_comparison < 0) {
                        free(out_buf_next);
                        successful_read = table->Read(bufferedNode->next[i], out_buf_curr);

                        if (!successful_read) {
                            break;
                        }
                    }

                    // If the next key is the key we are searching for, return it
                    else if (key_comparison == 0) {
                        free(out_buf_curr);
                        return nextBufferedNode;
                    }

                    // The next key value is greater than the desired key, we need to move down a level in the skip list
                    else {
                        free(out_buf_next);
                        break;
                    }
                }

                // Break out as the node is pointing to the tail at level i
                else {
                    break;
                }
            }
        }

        // If the node does not exist in the table, free the allocated memory
        free(out_buf_curr);

        // Return nullptr as key was not found
        return nullptr;
    }

    uint32_t PSkipList::ChooseHeight() {
        return distribution(engine);
    }

    bool PSkipList::Insert(char *key, RID rid) {
        // Use the Traverse function to reach the insertion point:
        // 1. if Traverse returns a valid node, then the key already exists in the skip
        //    list and return false;
        // 2. otherwise continue to:
        //    (a) determine the height of the new tower
        //    (b) build up the tower from bottom up using the vector passed in to the
        //        Traverse function: it should contain all the predecessors at each
        //        level
        //    (c) return true/false to indicate a successful/failed insert

        std::vector<RID> out_pred_nodes;
        PSkipListNode *node = Traverse(key, &out_pred_nodes);

        if (node) {
            // If the node was returned, it exists in the table
            // free the node to prevent memory leak
            free(node);
            return false;
        }

        // Get a height between 0 - 7
        uint32_t height_new_tower = ChooseHeight();

        // set the maximum height
        if (height < height_new_tower+1) {
            height = height_new_tower+1;
        }

        // Use kMaxLevels here as the largest kMaxLevel is the bottom level of the skiplist whereas the height variable
        // only knows the maximum height from the kMaxLevels variable to the tallest tower
        uint32_t current_list_level = PSkipListNode::kMaxLevels - 1;
        uint32_t height_tower_stop = current_list_level - height_new_tower;

        // Create the new node and insert it into the table
        PSkipListNode *new_node = NewNode(height_new_tower, key, rid);

        void *out_buf = (PSkipListNode*) malloc(sizeof(PSkipListNode) + key_size);

        // Move from bottom of the tower to the top
        while(current_list_level >= height_tower_stop) {
            RID previous_node_RID = out_pred_nodes.at(current_list_level);

            // Read in the previous node
            bool successful_read = table->Read(previous_node_RID, out_buf);
            if (!successful_read) {
                break;
            }

            auto prev_node = (PSkipListNode *) out_buf;

            new_node->next[current_list_level] = prev_node->next[current_list_level];
            prev_node->next[current_list_level] = new_node->TableRID;

            // Update the previous node so that changes persist in the table
            table->Update(previous_node_RID, (char *) prev_node);

            // Have this check here to prevent uint overflow
            if (current_list_level == 0) {
                break;
            }

            current_list_level--;
        }

        // Persist the changes to the new node
        table->Update(new_node->TableRID, (char *) new_node);

        // Now search for the key to make sure that the insertion was successful
//        RID SearchResult = Search(key);
//        bool node_exists = SearchResult.value == new_node->rid.value;

        free(out_buf);

        // If the node now exists, then insertion was successful, otherwise
        // return false
//        return node_exists;
        return true;
    }

    RID PSkipList::Search(char *key) {
        auto *node = Traverse(key);
        return node ? node->rid : RID();
    }

    bool PSkipList::Update(char *key, RID rid) {
        // Invoke the Traverse function to obtain the target tower and update its
        // payload RID. Returns true if the key is found and the RID is updated,
        // otherwise return false.
        auto *node = Traverse(key);

        if (node) {
            node->rid = rid;
            return table->Update(node->TableRID, (char *) node);
        }

        return false;
    }

    bool PSkipList::Delete(char *key) {
        // Similar to Insert(), use the Traverse function to obtain the target node
        // that contains the provided key and the predecessor nodes, then unlink the
        // returned node at all levels.
        //
        // The unlinked tower should be freed.
        //
        // Return true if the operation succeeded, false if the key is not found.

        std::vector<RID> previous_nodes;
        PSkipListNode* node = Traverse(key, &previous_nodes);

        if (node) {
            // Need to subtract 1 as the nLevels go from 0-7 whereas the height calculation is from 1-8
            uint32_t height_node = PSkipListNode::kMaxLevels - node->nlevels - 1;
            void *out_buf = (PSkipListNode*) malloc(sizeof(PSkipListNode) + key_size);

            // Iterate through all the levels so that the previous node can point to the node that will be deleted's
            // next TableRID value
            for (uint32_t i = height_node; i < PSkipListNode::kMaxLevels; i++) {
                PSkipListNode *previous_node = nullptr;
                table->Read(previous_nodes[i], out_buf);
                previous_node = (PSkipListNode *)out_buf;

                if (previous_node->next[i].IsValid()) {
                    previous_node->next[i] = node->next[i];
                }

                table->Update(previous_node->TableRID, (char *) previous_node);
            }

            table->Delete(node->TableRID);
            free(out_buf);

            return true;
        }

        return false;
    }

}  // namespace yase
