/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */

#include "../yase_internal.h"

namespace yase {

// Skip list node (tower)
struct SkipListNode {
  // Maximum allowed number of levels
  static const uint8_t kMaxLevels = 8;

  // Constant to represent invalid level
  static const uint8_t kInvalidLevels = 0;

  // Tower height
  uint32_t nlevels;

  // Payload (RID)
  RID rid;

  // Pointer to the next node. The i-th element represents the (i+1)th level
  SkipListNode *next[kMaxLevels];

  // Key (must be the last field of this struct)
  char key[0];

  // Constructor
  // @nlevels: tower height
  // @rid: payload RID
  SkipListNode(uint32_t nlevels, RID rid) : nlevels(nlevels), rid(rid) {}

  // Default constructor
  SkipListNode() : nlevels(kInvalidLevels) {}

  // Destructor
  ~SkipListNode() {}
};

// Skip list that maps keys to RIDs
class SkipList {
 public:
  // Constructor - create a skip list
  // @key_size: key size supported by this list
  SkipList(uint32_t key_size);

  // Destructor
  ~SkipList();

  // Compare two keys
  // @k1: key to compare
  // @k2: key to compare
  // Returns:
  //  < 0 if k1 < k2
  //  == 0 if k1 == k2
  //  > 0 if k1 > k2
  int CompareKey(char *k1, char *k2);

  // Insert a key - RID mapping into the skip list index
  // @key: pointer to the key
  // @rid: RID of the record represented by the key (the "data entry")
  bool Insert(char *key, RID rid);

  // Search for a key in the skip list
  // @key: pointer to the key
  // Returns the data entry corresponding to the search key; returns Invalid RID
  // if the key is not found
  RID Search(char *key);
  RID SearchLockFree(char *key);

    // Delete a key from the index
  // @key: pointer to the key
  // Returns true if the index entry is successfully deleted, false if the key
  // does not exist
  bool Delete(char *key);

  // Update the data entry with a new rid
  // @key: target key
  // @rid: new RID to be written
  // Returns true if the update was successful
  bool Update(char *key, RID rid);

  // Lock read or write each level
  void Lock_readwrite(uint32_t lock_code, uint32_t lock_heights, uint32_t lock_key, uint32_t lock_rid);

  // Unlock levels
  void Unlock_readwrite();

  //Initialize rwlock
  pthread_rwlock_t rwlock[SkipListNode::kMaxLevels];
  // Array for level locks for Debugging
  uint32_t level_lock[SkipListNode::kMaxLevels];

private:
  // Traverse the skip list to find a key
  // @key: pointer to the target key
  // @out_pred_nodes: a vector (provided by the caller) to store the predecessor
  // nodes during traversal), sorted by reverse-height order, i.e.,
  // out_pred_nodes[0] should point to the height-level predecessor (the head
  // dummy node), and out_pred_nodes[1] should point to the second height tower
  // and so on.
  // Returns the node containing the provided key if the key exists; otherwise
  // returns nullptr.
  SkipListNode *Traverse(char *key, std::vector<SkipListNode*> *out_pred_nodes = nullptr);

  uint32_t ChooseHeight();

  // Create a new skip list node (tower)
  // @levels: height of this tower
  // @key: pointer to the key
  // @rid: data entry (RID)
  SkipListNode *NewNode(uint32_t levels, char *key, RID rid);

 private:
  // key size supported
  uint32_t key_size;

  // Dummy head tower
  SkipListNode head;

  // Dummy tail tower
  SkipListNode tail;

  // Current height of the skip list
  uint32_t height;
};

}  // namespace yase
