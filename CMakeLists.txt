if (POLICY CMP0048)
  cmake_policy(SET CMP0048 NEW)
endif ()
cmake_minimum_required(VERSION 3.10)
project(yase)

# No in-source build
if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
  message(FATAL_ERROR "In-source builds are not allowed.")
endif("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")

enable_testing()

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror -lpthread -pthread -g")

add_subdirectory(Dummy)
add_subdirectory(BufferManager)
add_subdirectory(IndexManager)
add_subdirectory(LockManager)
add_subdirectory(Tests)
add_subdirectory(Benchmarks)

