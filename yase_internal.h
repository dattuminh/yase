/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#pragma once

#include <glog/logging.h>
#include <atomic>
#include <cstdlib>
#include <list>

namespace yase {
static const uint64_t MB = 1024 * 1024;

// Page ID - a 64-bit integer
struct PageID {
  // Represents an Invalid Page ID
  static const uint64_t kInvalidValue = ~uint64_t{0};

  // Structure of the Page ID:
  // ---16 bits---|---24 bits---|---24 bits---|
  //    File ID   |   Page Num  |    Unused   |
  uint64_t value;

  // Constructors
  PageID() : value(kInvalidValue) {}
  PageID(uint16_t file_id, uint32_t page_num) {
    value = (uint64_t) file_id << 48u | (uint64_t) page_num << 24u;
  }
  explicit PageID(uint64_t value) : value(value) {
    LOG_IF(FATAL, (value & 0xffffffu) != 0) << "invalid page_id";
  }

  inline bool IsValid() { return value != kInvalidValue; }
  inline uint16_t GetFileID() const { return value >> 48u; }
  inline uint32_t GetPageNum() const { return (value << 16u) >> 40u; }
  inline bool operator()(const PageID &l, const PageID &r) const { return l.value < r.value; }
};

// Record ID - a 64-bit integer
struct RID {
  // Represents an invalid RID
  static const uint64_t kInvalidValue = ~uint64_t{0};

  // Structure of the RID:
  // ---16 bits---|---24 bits---|---24 bits---|
  //    File ID   |   Page Num  |   Slot Num  |
  uint64_t value;

  // Constructors
  explicit RID(uint64_t value) : value(value) {}
  explicit RID() : value(kInvalidValue) {}
  explicit RID(PageID page_id, uint32_t slot_num) { value = page_id.value | slot_num; }

  inline PageID GetPageId() { return PageID{value & (~(uint64_t) 0xffffffu)}; }
  inline uint32_t GetSlotNum() { return (value << 40u) >> 40u; }
  inline bool IsValid() { return value != kInvalidValue; }

  static inline RID MakeRid(PageID page_id, uint32_t slot_num) {
    return RID(page_id.value | slot_num);
  }
};

struct LockRequest;
struct Transaction {
  static std::atomic<uint64_t> ts_counter;
  const static uint32_t kStateCommitted = 1;
  const static uint32_t kStateInProgress = 2;
  const static uint32_t kStateAborted = 3;

  // Transaction timestamp. Smaller == older.
  std::atomic<uint64_t> timestamp;

  // List of all locks *already acquired* by this transaction. The entries here
  // should be the lock request entries in the corresponding lock queues.
  std::list<LockRequest *> locks;

  // State of the transaction, must be one of the kState* values
  uint32_t state;

  Transaction() : timestamp(ts_counter.fetch_add(1)), state(kStateInProgress) {}
  ~Transaction() {}

  // Abort the transaction
  // Returns the timestamp of this transaction
  uint64_t Abort();

  // Commit the transaction
  void Commit();

  inline uint64_t GetTimestamp() { return timestamp; }
  inline bool IsAborted() { return state == kStateAborted; }
  inline bool IsCommitted() { return state == kStateCommitted; }
  inline bool IsInProgress() { return state == kStateInProgress; }
};

}  // namespace yase
