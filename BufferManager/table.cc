/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#include "table.h"
#include "../BufferManager/buffer_manager.h"


namespace yase {

Table::Table(std::string name, uint16_t file_id, uint32_t record_size, uint64_t max_data_size)
  : table_name(name), file(name, file_id, max_data_size), record_size(record_size) {
  next_free_pid = file.CreatePage(record_size);
}

RID Table::Insert(const char *record) {
  auto *bm = BufferManager::Get();

retry:
  PageID fpid = next_free_pid;
  Page *p = bm->PinPage(fpid);

  p->AcquireLatch();
  DataPage *dp = p->GetDataPage();
  uint32_t slot = 0;

  if (!dp->Insert(record, slot)) {
    // Lock the latch to check whether or not the next_free_pid value was changed
    latch.lock();

    bm->UnpinPage(p);
    p->ReleaseLatch();

    // If the value was not changed, i.e., it failed because no free slots
    if (next_free_pid.value == fpid.value) {
      auto new_pid = file.CreatePage(record_size);
      if (new_pid.IsValid()) {
          next_free_pid = new_pid;
          latch.unlock();
          goto retry;
      } else {
        // Have to search through the directory pages now
        new_pid = file.FindFreePage();
        if (new_pid.IsValid()) {
            next_free_pid = new_pid;
            latch.unlock();
            goto retry;
        } else {
            latch.unlock();
            // No space - return invalid RID
            return RID();
        }
      }
    } else {
      // Another thread changed the next_free_pid value because it was faster. Retry again
      latch.unlock();
      goto retry;
    }
  } else {
    p->SetDirty(true);
    bm->UnpinPage(p);
    p->ReleaseLatch();
  }

  // Mark the allocation in directory page
  latch.lock();
  static uint32_t entries_per_dir_page = PAGE_SIZE / sizeof(DirectoryPage::Entry);
  uint32_t dir_page_num = (fpid.GetPageNum() - file.GetDirPageCount()) / entries_per_dir_page;
  PageID dir_pid(file.GetId(), dir_page_num);

  p = bm->PinPage(dir_pid);
  p->AcquireLatch();
  latch.unlock();
  DirectoryPage *dirp = p->GetDirPage();
  uint32_t idx = (fpid.GetPageNum() - file.GetDirPageCount()) % entries_per_dir_page;

  LOG_IF(FATAL, dirp->entries[idx].free_slots == 0);
  --dirp->entries[idx].free_slots;
  p->SetDirty(true);
  bm->UnpinPage(p);
  p->ReleaseLatch();
  return RID(next_free_pid, slot);
}

bool Table::Read(RID rid, void *out_buf) {
  if (!file.PageExists(rid.GetPageId())) {
    return false;
  }

  auto *bm = BufferManager::Get();
  Page *p = bm->PinPage(rid.GetPageId());
  p->AcquireLatch();
  DataPage *dp = p->GetDataPage();
  bool success = dp->Read(rid, out_buf);
  bm->UnpinPage(p);
  p->ReleaseLatch();
  return success;
}

bool Table::Delete(RID rid) {
  auto *bm = BufferManager::Get();
  Page *p = bm->PinPage(rid.GetPageId());
  p->AcquireLatch();
  DataPage *dp = p->GetDataPage();
  bool success = dp->Delete(rid);
  if (success) {
    p->SetDirty(true);
  }
  bm->UnpinPage(p);
  p->ReleaseLatch();

  if (success) {
      // Mark the de-allocation in directory page
      static uint32_t entries_per_dir_page = PAGE_SIZE / sizeof(DirectoryPage::Entry);
      uint32_t dir_page_num = (rid.GetPageId().GetPageNum() - file.GetDirPageCount()) / entries_per_dir_page;
      PageID dir_pid(file.GetId(), dir_page_num);
      p = bm->PinPage(dir_pid);
      p->AcquireLatch();
      DirectoryPage *dirp = p->GetDirPage();

      uint32_t idx = (rid.GetPageId().GetPageNum() - file.GetDirPageCount()) % entries_per_dir_page;
      ++dirp->entries[idx].free_slots;
      p->SetDirty(true);
      bm->UnpinPage(p);
      p->ReleaseLatch();
  }

  return success;
}

bool Table::Update(RID rid, const char *record) {
  auto *bm = BufferManager::Get();
  Page *p = bm->PinPage(rid.GetPageId());
  p->AcquireLatch();
  DataPage *dp = p->GetDataPage();
  bool success = dp->Update(rid, record);
  p->SetDirty(true);
  bm->UnpinPage(p);
  p->ReleaseLatch();
  return success;
}



// Interfaces for key-based access:

// Insert a record to the table, returns the inserted record's RID
// @record: pointer to the record
// @key: a key to be inserted to the provided skip list index
// @index: skip list index to map key to the (new) RID
RID Table::Insert(const char *record, char *key, PSkipList *index) {
  auto *bm = BufferManager::Get();

retry:
  Page *p = bm->PinPage(next_free_pid);
  DataPage *dp = p->GetDataPage();
  uint32_t slot = 0;
  if (!dp->Insert(record, slot)) {
    bm->UnpinPage(p);
    auto new_pid = file.CreatePage(record_size);
    if (new_pid.IsValid()) {
      next_free_pid = new_pid;
      goto retry;
    } else {
      // Have to search through the directory pages now
      new_pid = file.FindFreePage();
      if (new_pid.IsValid()) {
        next_free_pid = new_pid;
        goto retry;
      } else {
        // No space - return invalid RID
        return RID();
      }
    }
  }
  p->SetDirty(true);
  bm->UnpinPage(p);

  // Mark the allocation in directory page
  static uint32_t entries_per_dir_page = PAGE_SIZE / sizeof(DirectoryPage::Entry);
  uint32_t dir_page_num = (next_free_pid.GetPageNum() - file.GetDirPageCount()) / entries_per_dir_page;
  PageID dir_pid(file.GetId(), dir_page_num);
  p = bm->PinPage(dir_pid);
  DirectoryPage *dirp = p->GetDirPage();

  uint32_t idx = (next_free_pid.GetPageNum() - file.GetDirPageCount()) % entries_per_dir_page;

  LOG_IF(FATAL, dirp->entries[idx].free_slots == 0);
  --dirp->entries[idx].free_slots;
  p->SetDirty(true);
  bm->UnpinPage(p);
  ////return RID(next_free_pid, slot);
  yase::RID rid = RID(next_free_pid, slot);
  if (index->Insert(key, rid)) {
    return rid;
  }
  return RID();
}


// Read a record with a given key
// @key: query key for the target record
// @index: skip list to query for the record's RID
// @out_buf: memory provided by user to store the read record
bool Table::Read(char *key, PSkipList *index, char *out_buf) {
  yase::RID rid = index->Search(key);
  if (rid.IsValid()) {
    if (!file.PageExists(rid.GetPageId())) {
      return false;
    }

    auto *bm = BufferManager::Get();
    Page *p = bm->PinPage(rid.GetPageId());
    DataPage *dp = p->GetDataPage();
    bool success = dp->Read(rid, out_buf);
    bm->UnpinPage(p);
    return success;
  }
  return false;
}
// Delete a record with the given key
// @key: key of the record to be deleted
// @index: skip list to query and delete the key-RID mapping from
bool Table::Delete(char *key, PSkipList *index) {
  yase::RID rid = index->Search(key);

  if (rid.IsValid()) {
    auto *bm = BufferManager::Get();
    Page *p = bm->PinPage(rid.GetPageId());
    DataPage *dp = p->GetDataPage();
    bool success = dp->Delete(rid);
    if (success) {
      p->SetDirty(true);
    }
    bm->UnpinPage(p);

    // Mark the dellocation in directory page
    static uint32_t entries_per_dir_page = PAGE_SIZE / sizeof(DirectoryPage::Entry);
    uint32_t dir_page_num = (rid.GetPageId().GetPageNum() - file.GetDirPageCount()) / entries_per_dir_page;
    PageID dir_pid(file.GetId(), dir_page_num);
    p = bm->PinPage(dir_pid);
    DirectoryPage *dirp = p->GetDirPage();

    uint32_t idx = (rid.GetPageId().GetPageNum() - file.GetDirPageCount()) % entries_per_dir_page;
    ++dirp->entries[idx].free_slots;
    p->SetDirty(true);
    bm->UnpinPage(p);

    if (index->Delete(key)) {
      return success;
    }
  }
  return false;
}

// Update a record with the given key, and the new record
// @key: key of the record to be updated
// @record: pointer to the new record value
// @index: skip list to query the record's RID with the given key
bool Table::Update(char *key, const char *record, PSkipList *index) {
  yase::RID rid = index->Search(key);
  if (rid.IsValid()) {
    auto *bm = BufferManager::Get();
    Page *p = bm->PinPage(rid.GetPageId());
    DataPage *dp = p->GetDataPage();
    bool success = dp->Update(rid, record);
    bm->UnpinPage(p);
    if (index->Update(key,rid)) {
      return success;
    }
  }
  return false;
}

}  // namespace yase
