/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#pragma once

#include <cstdlib>

#include "page.h"
#include "file.h"
#include "../IndexManager/pskiplist.h"


namespace yase {

// User-facing table abstraction
class Table {
 public:
  Table(std::string name, uint16_t file_id, uint32_t record_size, uint64_t max_data_size);
  ~Table() {}

  // Insert a record to the table, returns the inserted record's RID
  // @record: pointer to the record
  RID Insert(const char *record);

  // Read a record with a given RID
  // @rid: RID of the record to be read
  // @out_buf: memory provided by user to store the read record
  bool Read(RID rid, void *out_buf);

  // Delete a record with the given RID
  // @rid: RID of the record to be deleted
  bool Delete(RID rid);

  // Update a record with the given RID, and the new record
  // @rid: RID of the record to be updated
  // @record: pointer to the new record value
  bool Update(RID rid, const char *record);


  // Interfaces for key-based access:

  // Insert a record to the table, returns the inserted record's RID
  // @record: pointer to the record
  // @key: a key to be inserted to the provided skip list index
  // @index: skip list index to map key to the (new) RID
  RID Insert(const char *record, char *key, PSkipList *index);

  // Read a record with a given key
  // @key: query key for the target record
  // @index: skip list to query for the record's RID
  // @out_buf: memory provided by user to store the read record
  bool Read(char *key, PSkipList *index, char *out_buf);

  // Delete a record with the given key
  // @key: key of the record to be deleted
  // @index: skip list to query and delete the key-RID mapping from
  bool Delete(char *key, PSkipList *index);

  // Update a record with the given key, and the new record
  // @key: key of the record to be updated
  // @record: pointer to the new record value
  // @index: skip list to query the record's RID with the given key
  bool Update(char *key, const char *record, PSkipList *index);


 private:
  // The table's name
  std::string table_name;

  // The underlying file
  File file;

  // Set the PageID with free space
  PageID next_free_pid;

  // Record size supported by this table
  uint32_t record_size;

  // Latch protecting the table structure
  std::mutex latch;
};

}  // namespace yase
