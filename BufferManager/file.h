/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#pragma once

#include <cstdint>
#include "page.h"
#include <mutex>

namespace yase {

// Low-level primitives for reading and writing pages
class File {
 public:
  File(std::string &name, uint16_t file_id, uint64_t max_data_size);
  ~File();

  // Write a page to storage
  void FlushPage(void *page, PageID pid);

  // Load a page from storage
  void LoadPage(PageID page_id, void *out_buf);

  // Create a new page in the file permanently, returns the ID of the new page
  PageID CreatePage(uint16_t record_size);

  // Tell if the given page is created (or deleted) in the file
  bool PageExists(PageID pid);

  // Search the directory pages to find a page with free space to accomodate at
  // least one record
  PageID FindFreePage();

  inline uint32_t GetDirPageCount() { return dir_page_count; }
  inline uint16_t GetId() { return file_id; }

 private:
  // ID of this file
  uint16_t file_id;

  // Number of pages the file currently has, including page and dir pages
  uint32_t page_count;

  // File descriptor of the underlying file
  int fd;

  // Number of directory pages
  uint32_t dir_page_count;

  // Maximum size of the file, must be multiples of PAGE_SIZE 
  uint64_t max_size;

  // Latch protecting the file structure
  std::mutex latch;

};

}  // namespace yase
