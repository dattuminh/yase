/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#include <fcntl.h>
#include "file.h"
#include "buffer_manager.h"

namespace yase {
File::File(std::string &name, uint16_t file_id, uint64_t max_data_size) : file_id(file_id), page_count(0) {
  dir_page_count = max_data_size / PAGE_SIZE * sizeof(DirectoryPage::Entry) / PAGE_SIZE;
  LOG_IF(FATAL, dir_page_count == 0) << "Table is too small - cannot fill in one dir entry page";

  max_size = max_data_size + dir_page_count * PAGE_SIZE;
  LOG_IF(FATAL, max_size % PAGE_SIZE != 0) << "Data size is not multiples of page size (" << PAGE_SIZE << ")";

  std::lock_guard<std::mutex> lock(latch);
  fd = open(name.c_str(), O_CREAT | O_TRUNC | O_RDWR, 0644);
  LOG_IF(FATAL, fd < 0) << "Cannot create file for table " << name;

  // Initialize all directory pages
  for (uint32_t i = 0; i < max_size / PAGE_SIZE; ++i) {
    DirectoryPage::Entry entry;
    int written = write(fd, &entry, sizeof(entry));
    LOG_IF(FATAL, written != sizeof(DirectoryPage::Entry));
  }
  page_count = dir_page_count;

  // Register this file with buffer manager
  BufferManager::Get()->AddFileMapping(file_id, this);

  // Force data to storage
  fsync(fd);
}

File::~File() {
  fsync(fd);
  close(fd);
}

void File::FlushPage(void *page, PageID pid) {
//  std::lock_guard<std::mutex> lock(latch);
  int written = pwrite(fd, page, PAGE_SIZE, pid.GetPageNum() * PAGE_SIZE);
  LOG_IF(FATAL, written != PAGE_SIZE);
  fsync(fd);
}

void File::LoadPage(PageID page_id, void *out_buf) {
  // Extract the page number first
//  std::lock_guard<std::mutex> lock(latch);
  uint16_t page_num = page_id.GetPageNum();
  auto nread = pread(fd, out_buf, PAGE_SIZE, page_num * PAGE_SIZE);
  LOG_IF(FATAL, nread != PAGE_SIZE) << "Error reading file";
}

PageID File::CreatePage(uint16_t record_size) {
  static uint32_t entries_per_dir_page = PAGE_SIZE / sizeof(DirectoryPage::Entry);
  latch.lock();
  if (page_count + 1 > max_size / PAGE_SIZE) {
    latch.unlock();
    return PageID();
  }

  // Mark allocation in directory page
  PageID pid(file_id, page_count++);
  PageID dir_pid(file_id, (pid.GetPageNum() - dir_page_count) / entries_per_dir_page);
  auto *bm = BufferManager::Get();
  Page *dirp = bm->PinPage(dir_pid);
  dirp->AcquireLatch();

  DirectoryPage *dirpp = dirp->GetDirPage();
  uint32_t idx = (pid.GetPageNum() - dir_page_count) % entries_per_dir_page;
  dirpp->entries[idx].allocated = true;
  dirpp->entries[idx].free_slots = DataPage::GetMaxRecordCount(record_size);
  LOG_IF(FATAL, dirpp->entries[idx].free_slots == 0);
  bm->UnpinPage(dirp);
  dirp->ReleaseLatch();
  FlushPage(dirpp, dir_pid);

  // Initialize and write the data page
  Page *p = bm->PinPage(pid, true);
  p->AcquireLatch();
  latch.unlock();
  DataPage *dp = p->GetDataPage();
  memset((void *)dp, 0, PAGE_SIZE);
  new (dp) DataPage(record_size);
  bm->UnpinPage(p);
  p->ReleaseLatch();
  FlushPage(dp, pid);

  DLOG(INFO) << "Created page " << pid.GetPageNum();
  return pid;
}

bool File::PageExists(PageID pid) {
  std::lock_guard<std::mutex> lock(latch);
  return pid.GetPageNum() < page_count;
}

PageID File::FindFreePage() {
  for (uint32_t i = 0; i < dir_page_count; ++i) {
    PageID dir_pid(file_id, i);
    auto *bm = BufferManager::Get();
    Page *p = bm->PinPage(dir_pid);
    p->AcquireLatch();
    DirectoryPage *dirp = p->GetDirPage();

    // Loop over the entries to see which one has free space
    static uint32_t entries_per_dir_page = PAGE_SIZE / sizeof(DirectoryPage::Entry);
    for (uint32_t e = 0; e < entries_per_dir_page; ++e) {
      if (!dirp->entries[e].allocated || dirp->entries[e].free_slots > 0) {
        bm->UnpinPage(p);
        p->ReleaseLatch();
        return PageID(file_id, i * entries_per_dir_page + e + dir_page_count);
      }
    }
    bm->UnpinPage(p);
    p->ReleaseLatch();
  }
  return PageID();
}

}  // namespace yase
