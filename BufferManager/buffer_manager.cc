/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#include "buffer_manager.h"
#include <iostream>

namespace yase {

    BufferManager *BufferManager::instance = nullptr;
    std::vector<Page*> free_frames;

    // Initialize a new buffer manager
    BufferManager::BufferManager(uint32_t page_count) : page_count(page_count) {
        // Allocate and initialize memory for page frames
        // 1. malloc the desired amount of memory and store the address in
        //    member variable page_frames
        // 2. Clear the allocated memory to 0
        // 3. Use in-place new to initialize each frame

        page_frames = (Page *) malloc(page_count * PAGE_SIZE + sizeof(Page));
        memset((void *)page_frames, 0, PAGE_SIZE);

        for (uint32_t i = 0; i < page_count; i++) {
            new (&page_frames[i]) Page();
            free_frames.push_back(page_frames + i);
        }
    }

    BufferManager::~BufferManager() {
        for (uint32_t i = 0; i < page_count; i++) {
            Page* currentPage = page_frames + i;
            PageID pageId = currentPage->GetPageId();

            if (pageId.IsValid()){
                uint16_t fileId = pageId.GetFileID();
                File* file = file_map[fileId];
                file->FlushPage(currentPage->GetDataPage(), pageId);
            }
        }

        free_frames.clear();

        // Free page frames
        free(page_frames);
    }

    void BufferManager::FlushPage(Page *page) {
        // Flush the page back to storage
        PageID page_id_eviction = page->GetPageId();
        uint16_t file_id = page_id_eviction.GetFileID();

        File *file = file_map[file_id];
        file->FlushPage(page->GetDataPage(), page_id_eviction);
    }

    void BufferManager::LoadPage(Page *page, PageID page_id) {
        PageID page_id_eviction = page->GetPageId();
        uint16_t file_id = page_id_eviction.GetFileID();
        File *file = file_map[file_id];

        // Load the desired page back to into memory
        file->LoadPage(page_id, page->page_data);
    }

    void BufferManager::AssignPage(Page *page, PageID page_id) {
        page_map[page_id] = page;
        page->SetDirty(false);
        page->page_id = page_id;

        if (page->GetPinCount() == 0) {
            page->IncPinCount();
        }
    }

    Page* BufferManager::PinPage(PageID page_id, bool new_page) {
        // Pin a page in the buffer pool:
        // 1. Check if this is for creating a new page, if so, directly get a free page
        //    frame and set up the PageID-Page Frame mapping
        // 2. If this is not for creating a new page, see if the page is already
        //    buffered if so, return it; otherwise evict a page, load the desired page
        //    and set up the the new PageID-Page Frame mapping
        //
        // Note: before returning, make sure increment the page's pin_count (using
        // Page::IncPinCount function) and record page_id in the Page structure.
        //
        // The following return statement tries to silent compiler warning so the base
        // code compiles, you may need to revise/remove it in your implementation

        if (new_page) {
            latch.lock();

            // If there are no free frames available with a pin_count = 0, return nullptr
            if (free_frames.empty()) {
                latch.unlock();
                return nullptr;
            }

            // Take the first free page from the list and remove it
            Page* page = free_frames.front();
            free_frames.erase(free_frames.begin());
            latch.unlock();

            // Dont need to check for pin_count 0 as the vector only contains frames with pin_count 0
            if (page->is_dirty ||
                (page->GetPageId().IsValid() && page_map.count(page->GetPageId()))) {
                FlushPage(page);

                latch.lock();
                page_map.erase(page->GetPageId());
                latch.unlock();
            }

            AssignPage(page, page_id);
            return page;

        } else {
            // Is the pageId already mapped
            if (page_map.count(page_id) != 0) {
                Page* mappedPage = page_map[page_id];

                // if the page is in the free frames vector, remove it
                latch.lock();
                auto it = std::find(free_frames.begin(), free_frames.end(), mappedPage);
                if (it != free_frames.end()) {

                    // Page was found in the vector
                    int indexOfPage = std::distance(free_frames.begin(), it);
                    free_frames.erase(free_frames.begin() + indexOfPage);
                }
                latch.unlock();

                if (mappedPage->GetPinCount() == 0) {
                    mappedPage->IncPinCount();
                }

                return mappedPage;
            }

                // If it is not mapped, then find a page to evict
            else {
                latch.lock();

                // If there are no free frames available with a pin_count = 0, return nullptr
                if (free_frames.empty()) {
                    latch.unlock();
                    return nullptr;
                }

                // Take the first free page from the list and remove it
                Page* page = free_frames.front();
                free_frames.erase(free_frames.begin());
                latch.unlock();

                // Dont need to check for pin_count 0 as the vector only contains frames with pin_count 0
                if (page->is_dirty) {
                    PageID page_to_evict = page->GetPageId();
                    FlushPage(page);
                    LoadPage(page, page_id);

                    // Flush the current page and load the desired one
                    latch.lock();
                    page_map.erase(page_to_evict);
                    latch.unlock();
                }

                AssignPage(page, page_id);
                return page;
            }
        }
    }

    void BufferManager::UnpinPage(Page *page) {
        // Unpin the provided page, by decrementing the pin count. The page should
        // stay in the buffer pool until another operation evicts it later.
        latch.lock();
        if (page->pin_count > 0) {
            page->DecPinCount();
        }

        if (page->pin_count == 0) {
            free_frames.push_back(page);
        }
        latch.unlock();
    }

    void BufferManager::AddFileMapping(uint16_t file_id, File* file) {
        // Add the mapping between file_id and file to a std::map using file_id as the
        // key
        latch.lock();
        file_map[file_id] = file;
        latch.unlock();
    }

}  // namespace yase
