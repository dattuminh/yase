/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#pragma once

#include <glog/logging.h>
#include <cstdlib>

#include "../yase_internal.h"

#define PAGE_SIZE 4096

namespace yase {

class DataPage {
 private:
  // Data area, including actual data that starts from data[0], and the bit array
  // (excluding record count) that precedes record count and record size
  // information which are at the end of the page
  char data[PAGE_SIZE - sizeof(uint16_t) * 2];

  // Number of valid records (occupied slots), located at the end of the page
  uint16_t record_count;

  // Record size
  uint16_t record_size;

  // Set the bit in the bit arrary for the given slot ID
  void SetBitArray(uint32_t slot_id, bool value);

 public:
  DataPage() : data{0}, record_count(0), record_size(0) {}
  DataPage(uint16_t record_size) : data{0}, record_count(0), record_size(record_size) {}

  // Returns true if the given slot is occupied
  bool SlotOccupied(uint16_t slot);

  // Retrieve a record with a given RID from the page 
  bool Read(RID rid, void *out_buf);

  // Insert a new record
  bool Insert(const char *record, uint32_t &out_slot_id);

  // Delete a record by a given RID
  bool Delete(RID rid);

  // Update a record with the given RID
  bool Update(RID rid, const char *new_record);

  // Return the maximum number of records that can be stored in this page
  static uint16_t GetMaxRecordCount(uint32_t record_size);
};

// Directory page that includes one entry per data page
struct DirectoryPage {
  struct Entry {
    uint16_t free_slots;
    bool allocated;
    Entry() : free_slots(0) , allocated(false) {}
  };

  Entry entries[PAGE_SIZE / sizeof(Entry)];

  static_assert(PAGE_SIZE % sizeof(Entry) == 0, "Page size not a multiple of Entry size");
};

static_assert(sizeof(DataPage) == PAGE_SIZE, "Wrong data page size");
static_assert(sizeof(DirectoryPage) == PAGE_SIZE, "Wrong dir page size");
}  // namespace yase
