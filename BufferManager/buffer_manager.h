/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#pragma once
#include <algorithm>
#include <list>
#include <memory>
#include <map>
#include <mutex>

#include "file.h"

namespace yase {

    class BufferManager;

    // Representation of a page in memory. The buffer has an arry of Pages to
    // accommodate DataPages and DirectoryPages.
    class Page {
      friend class BufferManager;

     private:
      // Whether the page is dirty
      bool is_dirty;

      // Pin count - the number of users of this page
      uint16_t pin_count;

      // ID of the page held in page_data
      PageID page_id;

      // Latch protecting this page frame
      std::mutex latch;

      // Space to hold a real page loaded from storage
      char page_data[PAGE_SIZE];

     public:
      Page() : is_dirty(false), pin_count(0) {}
      ~Page() {}

      inline DataPage *GetDataPage() { return (DataPage *)page_data; }
      inline DirectoryPage *GetDirPage() { return (DirectoryPage *)page_data; }
      inline void SetDirty(bool dirty) { is_dirty = dirty; }
      inline PageID GetPageId() { return page_id; }
      inline void AcquireLatch() { latch.lock(); };
      inline void ReleaseLatch() { latch.unlock(); }

     private:
      inline void IncPinCount() { pin_count += 1; }
      inline void DecPinCount() { pin_count -= 1; }
      inline bool IsDirty() { return is_dirty; }
      inline uint16_t GetPinCount() { return pin_count; }
    };

    class BufferManager {
     private:
      static BufferManager *instance;

     public:
      inline static void Initialize(uint32_t page_count) {
        BufferManager::instance = new BufferManager(page_count);
      }
      inline static BufferManager *Get() { return instance; }

      inline static void Destruct(BufferManager *bm) {
          bm->~BufferManager();
      }

      BufferManager(BufferManager const &) = delete;
      void operator=(BufferManager const &) = delete;

      // Pin a page
      // @page_id: ID of the page to pin
      // @new_page: whether this is for creating a new page that doesn't have exist
      //            in permanent storage yet
      // Returns a page frame containing the pinned page
      Page *PinPage(PageID page_id, bool new_page = false);

      // Unpin a page
      // @page: Page to unpin
      void UnpinPage(Page *page);

      // Flush a page
      // @page: page to write back to storage
      // @needToLoad: Determines whether page_map removal of page
      void FlushPage(Page* page);

      // Load a page into memory from storage
      // @pageId: pageId of the page that we wish to load in from memory
      // @page: Page that will be used to load in the data
      void LoadPage(Page* page, PageID page_id);

      // Assigns variables required to hand off current page mapping to new one
      // @page: Page to have its mapping re-assigned to another frame
      // @page_id: Id of the new page that is getting mapped to @page
      void AssignPage(Page* page, PageID page_id);

      // Add a file ID - File* mapping to support multiple tables
      // @file_id: ID of the file
      // @file: pointer to the file object
      void AddFileMapping(uint16_t file_id, File *file);

     private:
      // Buffer manager constructor
      // @page_count: number of pages in the buffer pool
      BufferManager(uint32_t page_count);

      ~BufferManager();

      // File ID - File* mapping
      std::map<uint16_t, File*> file_map;

      // Page ID (file-local) - Page frame mapping
      std::map<PageID, Page*, PageID> page_map;

      // Number of page frames
      uint32_t page_count;

      // Latch protecting the buffer manager
      std::mutex latch;

        // An array of buffer pages
      Page *page_frames;
    };
}  // namespace yase
