#include <assert.h>
#include <iostream>
#include <memory>
#include <cstdio>
#include <thread>

#include "../BufferManager/buffer_manager.h"
#include "../BufferManager/table.h"
#include "../IndexManager/skiplist.h"

#include <glog/logging.h>
#include <gtest/gtest.h>

static const uint32_t kRecordSize = 8;
static const uint32_t kDataSize = 4 * yase::MB;
static const uint16_t kFileId = 1;
static const uint32_t kPageCount = 50;

// Number of concurrent insert threads
static const uint32_t kThreads = 4;

void InsertRecord(uint32_t thread_id, yase::Table *table) {
    uint16_t max_nrecs_per_page = yase::DataPage::GetMaxRecordCount(kRecordSize);
    // With a 4MB max-size table, there is only one directory page needed, so data
    // page number starts at 1
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1; ++p) {
        // Insert some records
        for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
            if (i % kThreads == thread_id) {
                char *record = (char *)&i;
                yase::RID rid = table->Insert(record);
                ASSERT_TRUE(rid.IsValid());
                //ASSERT_EQ(rid.GetSlotNum(), i % max_nrecs_per_page);
                //ASSERT_EQ(rid.GetPageId().GetPageNum(), p);
            }
        }
    }
}

//Read from the skiplist
void ReadRecord(uint32_t thread_id, yase::Table *table){
    uint16_t max_nrecs_per_page = yase::DataPage::GetMaxRecordCount(kRecordSize);
    uint64_t value = 0;
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1; ++p) {
        // Insert some records
        for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
            if (i % kThreads == thread_id) {
                yase::RID rid(yase::PageID(kFileId, p), i);
                bool success = table->Read(rid, (void *)&value);
                ASSERT_TRUE(success);
            }
        }
    }
}

//Read from the skiplist
void UpdateRecord(uint32_t thread_id, yase::Table *table){
    uint16_t max_nrecs_per_page = yase::DataPage::GetMaxRecordCount(kRecordSize);
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1; ++p) {
        // Insert some records
        for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
            if (i % kThreads == thread_id) {
                uint64_t v = i + 27;
                char *record = (char *)&v;
                yase::RID rid(yase::PageID(kFileId, p), i);
                bool success = table->Update(rid, record);
                ASSERT_TRUE(success);

                // Read it back
                success = table->Read(rid, record);
                ASSERT_TRUE(success);
                ASSERT_EQ(*(uint64_t*)record, v);
            }
        }
    }
}

void DeleteRecord(uint32_t thread_id, yase::Table *table){
    uint16_t max_nrecs_per_page = yase::DataPage::GetMaxRecordCount(kRecordSize);
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1; ++p) {
        // Insert some records
        for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
            if (i % kThreads == thread_id) {
                yase::RID rid(yase::PageID(kFileId, p), i);
                bool success = table->Delete(rid);
                ASSERT_TRUE(success);
            }
        }
    }
}

//Insert into the skiplist
void InsertRid(uint32_t thread_id, yase::SkipList *slist){
    for (uint64_t k = 0; k <= 400; ++k){
        if(k%kThreads == thread_id){
            yase::RID rid(k*2);
            bool success = slist->Insert((char *)&k, rid);
            ASSERT_TRUE(success);
        }
    }
}

//Update entries in the skiplist
void UpdateRid(uint32_t thread_id, yase::SkipList *slist){
    for (uint64_t k = 0; k <= 400; ++k){
        if(k%kThreads == thread_id){
            yase::RID rid(k*3);
            bool success = slist->Update((char *)&k, rid);
            ASSERT_TRUE(success);
        }
    }
}

//Delete entries in the skiplist
void DeleteRid(uint32_t thread_id, yase::SkipList *slist){
    for (uint64_t k = 0; k <= 400; ++k){
        if(k%kThreads == thread_id){
            bool success = slist->Delete((char *)&k);
            ASSERT_TRUE(success);
            // Read should fail now
            yase::RID r = slist->Search((char *)&k);
            ASSERT_FALSE(r.IsValid());
        }
    }
}

GTEST_TEST(Table, TableTest) {
    // Initialize the buffer pool with 50 page frames
    yase::BufferManager::Initialize(kPageCount);

    // Create a table
    yase::Table table("test_file", kFileId, kRecordSize, kDataSize);

    // Start a bunch of threads to do inserts concurrently
    std::vector<std::thread *> threads;
    for (uint32_t i = 0; i < kThreads; ++i) {
        threads.push_back(new std::thread(InsertRecord, i, &table));
    }

    for (auto &t : threads) {
        t->join();
        delete t;
    }
    threads.clear();

    for (uint32_t i = 0; i < kThreads; ++i) {
        threads.push_back(new std::thread(ReadRecord, i, &table));
    }

    for (auto &t : threads) {
        t->join();
        delete t;
    }
    threads.clear();

    for (uint32_t i = 0; i < kThreads; ++i) {
        threads.push_back(new std::thread(UpdateRecord, i, &table));
    }

    for (auto &t : threads) {
        t->join();
        delete t;
    }
    threads.clear();

    for (uint32_t i = 0; i < kThreads; ++i) {
        threads.push_back(new std::thread(DeleteRecord, i, &table));
    }

    // Wait for all threads to finish
    for (auto &t : threads) {
        t->join();
        delete t;
    }
    threads.clear();

    yase::BufferManager::Destruct(yase::BufferManager::Get());
}

GTEST_TEST(SkipList, SkipListTest) {
    // Initialize a skip list that supports 8-byte key
    yase::SkipList slist(8);

    //start threads for concurrent insertion
    std::vector<std::thread *> threads;
    for (uint32_t i = 0; i < kThreads; ++i) {
        threads.push_back(new std::thread(InsertRid, i, &slist));
    }

    // Wait for all threads to finish
    for (auto &t : threads) {
        t->join();
        delete t;
    }
    threads.clear();

//    for (uint32_t i = 0; i < kThreads; ++i) {
//        threads.push_back(new std::thread(UpdateRid, i, &slist));
//    }
//
//    // Wait for all threads to finish
//    for (auto &t : threads) {
//        t->join();
//        delete t;
//    }
//    threads.clear();
//
//    for (uint32_t i = 0; i < kThreads; ++i) {
//        threads.push_back(new std::thread(DeleteRid, i, &slist));
//    }
//
//    // Wait for all threads to finish
//    for (auto &t : threads) {
//        t->join();
//        delete t;
//    }
//    threads.clear();
}

int main(int argc, char **argv) {
    ::google::InitGoogleLogging(argv[0]);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
