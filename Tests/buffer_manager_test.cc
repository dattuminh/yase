#include <assert.h>
#include <iostream>
#include <memory>
#include <cstdio>
#include "../BufferManager/buffer_manager.h"
#include "../BufferManager/table.h"

#include <glog/logging.h>
#include <gtest/gtest.h>

GTEST_TEST(Table, TableTest) {
    static const uint32_t kRecordSize = 8;
    static const uint32_t kDataSize = 4 * yase::MB;
    static const uint16_t kFileId = 1;
    static const uint32_t kPageCount = 50;
    
    // Initialize the buffer pool with 50 page frames
    yase::BufferManager::Initialize(kPageCount);

    // Create a table
    yase::Table table("test_file", kFileId, kRecordSize, kDataSize);
    uint16_t max_nrecs_per_page = yase::DataPage::GetMaxRecordCount(kRecordSize);

    // With a 4MB max-size table, there is only one directory page needed, so data
    // page number starts at 1
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1; ++p) {
        // Insert some records
        for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
            char *record = (char *)&i;
            yase::RID rid = table.Insert(record);
            
            ASSERT_TRUE(rid.IsValid());
            ASSERT_EQ(rid.GetSlotNum(), i % max_nrecs_per_page);
            ASSERT_EQ(rid.GetPageId().GetPageNum(), p);
        }
    }

    // More inserts should fail
    uint64_t i = 1000;
    char *record = (char *)&i;
    yase::RID rid = table.Insert(record);
    ASSERT_EQ(rid.IsValid(), false);


    // Read back the values inserted, and delete the ones with even slot numbers
    uint64_t value = 0;
    uint32_t deleted = 0;
    std::vector<yase::RID> deleted_rids;
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1; ++p) {
        for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
            yase::RID rid(yase::PageID(kFileId, p), i);

            bool success = table.Read(rid, (void *)&value);

            ASSERT_TRUE(success);
            ASSERT_EQ(value, i);

            if (i % 2 == 0) {
                success = table.Delete(rid);
                ASSERT_TRUE(success);
                ++deleted;
            }
        }
    }

    // Do some updates with odd-numbered records
    uint32_t p = 1;
    for (uint64_t i = 1; i < max_nrecs_per_page; i += 2) {
        uint64_t v = i + 27;
        char *record = (char *)&v;
        yase::RID rid(yase::PageID(kFileId, p), i);
        bool success = table.Update(rid, record);
        ASSERT_TRUE(success);

        // Read it back
        success = table.Read(rid, record);
        ASSERT_TRUE(success);
        ASSERT_EQ(*(uint64_t*)record, i + 27);
    }

    // Now insert more records, reusing the deleted slots
    for (uint64_t i = 0; i < deleted; ++i) {
        char *record = (char *)&i;
        yase::RID r = table.Insert(record);
        ASSERT_TRUE(r.IsValid());
    }

    yase::BufferManager::Destruct(yase::BufferManager::Get());
}

// Dual table test
GTEST_TEST(Table, DualTableTest) {
    // 1st table ===================================================
    uint32_t kRecordSize = 16;
    uint32_t kDataSize = 4 * yase::MB;
    uint16_t kFileId = 1;
    uint32_t kPageCount = 50;


    // Initialize the buffer pool with 50 page frames
    yase::BufferManager::Initialize(kPageCount);

    // Create a table
    yase::Table table1("test_file1", kFileId, kRecordSize, kDataSize);
    uint16_t max_nrecs_per_page = yase::DataPage::GetMaxRecordCount(kRecordSize);

    // With a 4MB max-size table, there is only one directory page needed, so data
    // page number starts at 1
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1; ++p) {
        // Insert some records
        for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
            char *record = (char *)&i;
            yase::RID rid = table1.Insert(record);

            ASSERT_TRUE(rid.IsValid());
            ASSERT_EQ(rid.GetSlotNum(), i % max_nrecs_per_page);
            ASSERT_EQ(rid.GetPageId().GetPageNum(), p);
        }
    }

    // table1 test start ==============================================
    // More inserts should fail
    uint64_t i = 1000;
    char *record = (char *)&i;
    yase::RID rid = table1.Insert(record);
    ASSERT_EQ(rid.IsValid(), false);

    // Read back the values inserted, and delete the ones with even slot numbers
    uint64_t value = 0;
    uint32_t deleted = 0;
    std::vector<yase::RID> deleted_rids;
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1; ++p) {
        for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
            yase::RID rid(yase::PageID(kFileId, p), i);

            bool success = table1.Read(rid, (void *)&value);
            ASSERT_TRUE(success);
            ASSERT_EQ(value, i);

            if (i % 2 == 0) {

                success = table1.Delete(rid);
                ASSERT_TRUE(success);
                ++deleted;
            }

        }
    }

    // Do some updates with odd-numbered records
    uint32_t p = 1;
    for (uint64_t i = 1; i < max_nrecs_per_page; i += 2) {
        uint64_t v = i + 27;
        char *record = (char *)&v;
        yase::RID rid(yase::PageID(kFileId, p), i);
        bool success = table1.Update(rid, record);
        ASSERT_TRUE(success);

        // Read it back
        success = table1.Read(rid, record);
        ASSERT_TRUE(success);
        ASSERT_EQ(*(uint64_t*)record, v);
    }


    // Now insert more records, reusing the deleted slots
    for (uint64_t i = 0; i < deleted; ++i) {
        char *record = (char *)&i;
        yase::RID r = table1.Insert(record);
        ASSERT_TRUE(r.IsValid());
    }

    yase::BufferManager::Destruct(yase::BufferManager::Get());
    // table1 test end  ================================


    // table2 test start ================================
    uint32_t kRecordSize2 = 8;
    uint32_t kDataSize2 = 4 * yase::MB;
    uint16_t kFileId2 = 1;
    uint32_t kPageCount2 = 50;

    // 2nd table  =====================================================
    // Initialize the buffer pool with 50 page frames
    yase::BufferManager::Initialize(kPageCount2);

    // Create a table
    yase::Table table2("test_file2", kFileId2, kRecordSize2, kDataSize2);
    uint16_t max_nrecs_per_page2 = yase::DataPage::GetMaxRecordCount(kRecordSize2);

    // With a 4MB max-size table, there is only one directory page needed, so data
    // page number starts at 1
    for (uint32_t p = 1; p < kDataSize2 / PAGE_SIZE + 1; ++p) {
        // Insert some records
        for (uint64_t i = 0; i < max_nrecs_per_page2; ++i) {
            char *record = (char *)&i;
            yase::RID rid = table2.Insert(record);

            ASSERT_TRUE(rid.IsValid());
            ASSERT_EQ(rid.GetSlotNum(), i % max_nrecs_per_page2);
            ASSERT_EQ(rid.GetPageId().GetPageNum(), p);
        }
    }

    // More inserts should fail
    uint64_t i2 = 1000;
    char *record2 = (char *)&i2;
    yase::RID rid2 = table2.Insert(record2);
    ASSERT_EQ(rid2.IsValid(), false);

    // Read back the values inserted, and delete the ones with even slot numbers
    uint64_t value2 = 0;
    uint32_t deleted2 = 0;
    std::vector<yase::RID> deleted_rids2;
    for (uint32_t p = 1; p < kDataSize2 / PAGE_SIZE + 1; ++p) {
        for (uint64_t i = 0; i < max_nrecs_per_page2; ++i) {
            yase::RID rid(yase::PageID(kFileId2, p), i);

            bool success = table2.Read(rid, (void *)&value2);

            ASSERT_TRUE(success);
            //ASSERT_EQ(value2, i);

            if (i % 2 == 0) {

                success = table2.Delete(rid);
                ASSERT_TRUE(success);
                ++deleted2;
            }
        }
    }

    // Do some updates with odd-numbered records
    uint32_t p2 = 1;
    for (uint64_t i = 1; i < max_nrecs_per_page2; i += 2) {
        uint64_t v = i + 27;
        char *record = (char *)&v;
        yase::RID rid(yase::PageID(kFileId2, p2), i);
        bool success = table2.Update(rid, record);
        ASSERT_TRUE(success);

        // Read it back
        success = table2.Read(rid, record);
        ASSERT_TRUE(success);
        ASSERT_EQ(*(uint64_t*)record, v);
    }

    // Now insert more records, reusing the deleted slots
    for (uint64_t i = 0; i < deleted2; ++i) {
        char *record = (char *)&i;
        yase::RID r = table2.Insert(record);
        ASSERT_TRUE(r.IsValid());
    }

    yase::BufferManager::Destruct(yase::BufferManager::Get());
}

int main(int argc, char **argv) {
    ::google::InitGoogleLogging(argv[0]);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
