#include <assert.h>
#include <iostream>
#include <memory>
#include <cstdio>
#include "../IndexManager/skiplist.h"
//#include "../IndexManager/pskiplist.h"
#include "../BufferManager/table.h"

#include <glog/logging.h>
#include <gtest/gtest.h>

// Raw in-memory skip list functionality test
GTEST_TEST(SkipList, SkipListTest) {
    // Initialize a skip list that supports 8-byte key
    yase::SkipList slist(8);

    // Insert 10000 keys
    static const uint64_t kKeys = 10000;
    for (uint64_t k = 1; k <= kKeys; ++k) {
        yase::RID rid(k * 2);
        bool success = slist.Insert((char *)&k, rid);
        ASSERT_TRUE(success);
    }
    LOG(INFO) << "Insert succeeded";


    // Read the inserted keys back
    for (uint64_t k = 1; k <= kKeys; ++k) {
        yase::RID r = slist.Search((char *)&k);
        ASSERT_EQ(r.value, yase::RID(k * 2).value);
    }

    LOG(INFO) << "Read succeeded";

    // Delete some inserted keys
    for (uint64_t k = 0; k < kKeys; ++k) {
        if (k % 2) {
            bool success = slist.Delete((char *)&k);
            ASSERT_TRUE(success);
            // Read should fail now
            yase::RID r = slist.Search((char *)&k);
            ASSERT_FALSE(r.IsValid());
        }
    }
}

GTEST_TEST(PSkipList, PSkipListTest) {
    static const uint32_t kPageCount = 50;

    // Initialize the buffer pool with 50 page frames
    yase::BufferManager::Initialize(kPageCount);

    // Initialize a skip list that supports 8-byte key
    yase::PSkipList plist("PSkipList", 8);

    // Insert 10000 keys
    static const uint64_t kKeys = 5000;
    for (uint64_t k = 1; k <= kKeys; ++k) {
        yase::RID rid(k * 2);
        bool success = plist.Insert((char *) &k, rid);
        ASSERT_TRUE(success);
    }

    LOG(INFO) << "Insert succeeded";

    // Read the inserted keys back
    for (uint64_t k = 1; k <= kKeys; ++k) {
        yase::RID r = plist.Search((char *) &k);
        ASSERT_EQ(r.value, yase::RID(k * 2).value);
    }

    LOG(INFO) << "Read succeeded";

    for (uint64_t k = 1; k <= kKeys; ++k) {
        yase::RID rid(k * 3);
        bool success = plist.Update((char *)&k, rid);
        ASSERT_TRUE(success);

        yase::RID r = plist.Search((char *)&k);
        ASSERT_EQ(r.value, yase::RID(k * 3).value);
    }

    LOG(INFO) << "Update succeeded";

    // Delete some inserted keys
    for (uint64_t k = 0; k < kKeys; ++k) {
        if (k % 2) {
            bool success = plist.Delete((char *) &k);
            ASSERT_TRUE(success);
            // Read should fail now
            yase::RID r = plist.Search((char *) &k);
            ASSERT_FALSE(r.IsValid());
        }
    }

    LOG(INFO) << "Delete Succeeded";

    yase::BufferManager::Destruct(yase::BufferManager::Get());
}

GTEST_TEST(PSkipList, PSkipListTabletest) {

    static const uint32_t kRecordSize = 8;
    static const uint64_t kDataSize = 4 * yase::MB;
    static const uint16_t kFileId = 1;
    static const uint32_t kPageCount = 50;

    // Initialize the buffer pool with 50 page frames
    yase::BufferManager::Initialize(kPageCount);

    // Create a table
    yase::Table table3("test_file", kFileId, kRecordSize, kDataSize);

    // Initialize a skip list that supports 8-byte key
    yase::PSkipList plist3("PSkipList", 8);

    uint16_t max_nrecs_per_page = yase::DataPage::GetMaxRecordCount(kRecordSize);

    // With a 4MB max-size table, there is only one directory page needed, so data
    // page and key number starts at 1
    uint64_t key=1;
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1 && p<3; ++p) {
      // Insert some records
      for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {
        char *record = (char *)&i;

        yase::RID rid = table3.Insert(record, (char *)&key, &plist3);

        ASSERT_TRUE(rid.IsValid());
        ASSERT_EQ(rid.GetSlotNum(), i % max_nrecs_per_page);
        ASSERT_EQ(rid.GetPageId().GetPageNum(), p);
        key++;
      }
    }


    // Read back the values inserted, and delete the ones with even key numbers
    uint64_t value = 0;
    uint32_t deleted = 0;

    key =1;
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1 && p < 3; ++p) {
      for (uint64_t i = 0; i < max_nrecs_per_page; ++i) {

        char* cvalue =(char *)&value;
        bool success = table3.Read((char *)&key, &plist3, cvalue);
        ASSERT_TRUE(success);
        ASSERT_EQ(value, i);

        if (key % 2 == 0) {
          success = table3.Delete((char *)&key, &plist3);
          ASSERT_TRUE(success);
          ++deleted;
        }
        key++;
      }
    }
    // Do some updates with odd-numbered keys
    uint64_t key1 = 1;
    uint64_t value2 = 0;
    uint64_t v = key1 + 1;
    for (uint32_t p = 1; p < 2; ++p) {
      if(key%2 == 0){
      for (uint64_t i = 1; i < max_nrecs_per_page; i += 1) {
        char* cvalue2 =(char *)&value2;
        v = key1 + 1;
        char *record2 = (char *)&v;
        bool success = table3.Update((char *)&key1, record2, &plist3);
        if(!success){
        ASSERT_TRUE(success);
        }

      // Read it back
        success = table3.Read((char *)&key1, &plist3, cvalue2);

        ASSERT_EQ(value2, v);
        key1 += 1;
      }
    }
    }
    // Now insert more, using the deleted keys
    uint64_t key3 = 2;
    for (uint32_t p = 1; p < kDataSize / PAGE_SIZE + 1 && p <2 ; ++p) {
      for (uint64_t i = 0;  i < max_nrecs_per_page; i += 2) {
        char *record = (char *)&i;
        yase::RID rid = table3.Insert(record, (char *)&key3, &plist3);

        ASSERT_TRUE(rid.IsValid());
        key3+=2;
      }
    }

    yase::BufferManager::Destruct(yase::BufferManager::Get());
}


int main(int argc, char **argv) {
  ::google::InitGoogleLogging(argv[0]);
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
