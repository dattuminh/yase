#include <assert.h>
#include <iostream>
#include <memory>
#include <cstdio>
#include <thread>

#include "../LockManager/lock_manager.h"
#include "../BufferManager/table.h"

#include <glog/logging.h>
#include <gtest/gtest.h>

// Single lock basic functionality test
GTEST_TEST(Lock, LockTest) {
    yase::LockManager::Initialize();

    // Create transactions to acquire and release locks
    yase::Transaction t;

    // Fake RIDs
    std::vector<yase::RID> rids;
    uint64_t kRids = 1000;
    for (uint64_t i = 0; i < kRids; ++i) {
        rids.emplace_back(i + 1);
    }

    auto *lockmgr = yase::LockManager::Get();

    // Shared lock
    for (uint32_t i = 0; i < rids.size(); ++i) {
        bool acquired = lockmgr->AcquireLock(&t, rids[i], yase::LockRequest::Mode::SH);
        ASSERT_TRUE(acquired);
        lockmgr->ReleaseLock(&t, rids[i]);
    }

    //  X locks
    for (uint32_t i = 0; i < rids.size(); ++i) {
        bool acquired = lockmgr->AcquireLock(&t, rids[i], yase::LockRequest::Mode::XL);
        ASSERT_TRUE(acquired);
        lockmgr->ReleaseLock(&t, rids[i]);
    }
}

// Helper function for deadlock test
void DdlThread(yase::LockManager *lm, yase::Transaction *t, yase::RID *rid, yase::LockRequest::Mode mode) {
    if (!lm->AcquireLock(t, *rid, mode)) {
        t->Abort();
    }
}

// Testing deadlock
GTEST_TEST(Lock, WaitDieTest) {
    yase::LockManager::Initialize();

    // Create two transaction to acquire and release locks
    yase::Transaction t1;
    yase::Transaction t2;

    ASSERT_TRUE(t2.GetTimestamp() >  t1.GetTimestamp());

    // Fake RID
    yase::RID rid1(0xbeef);
    yase::RID rid2(0xdead);

    auto *lockmgr = yase::LockManager::Get();

    // T1 locks rid1 in SH mode
    bool acquired = lockmgr->AcquireLock(&t1, rid1, yase::LockRequest::Mode::SH);
    ASSERT_TRUE(acquired);

    // T2 locks rid2 in XL mode
    acquired = lockmgr->AcquireLock(&t2, rid2, yase::LockRequest::Mode::XL);
    ASSERT_TRUE(acquired);

    ASSERT_TRUE(t1.IsInProgress());
    ASSERT_TRUE(t2.IsInProgress());

    // Start two threads to create a deadlock
    // T1 is older (smaller timestamp), so it will wait
    std::thread thread1(DdlThread, lockmgr, &t1, &rid2, yase::LockRequest::Mode::SH);

    // T2 is younger, so it will not wait
    std::thread thread2(DdlThread, lockmgr, &t2, &rid1, yase::LockRequest::Mode::XL);

    thread1.join();
    thread2.join();

    // T2 should have been aborted
    ASSERT_TRUE(t2.IsAborted());
    t1.Commit();
    ASSERT_TRUE(t1.IsCommitted());
}

int main(int argc, char **argv) {
    ::google::InitGoogleLogging(argv[0]);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
