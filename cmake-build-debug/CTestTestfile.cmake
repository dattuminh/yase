# CMake generated Testfile for 
# Source directory: /home/sukhdeep/Desktop/CMPT 454/yase
# Build directory: /home/sukhdeep/Desktop/CMPT 454/yase/cmake-build-debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Dummy")
subdirs("BufferManager")
subdirs("IndexManager")
subdirs("LockManager")
subdirs("Tests")
