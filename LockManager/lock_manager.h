/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#pragma once
#include <algorithm>
#include <list>
#include <memory>
#include <map>
#include <mutex>
#include <unordered_map>

#include "../yase_internal.h"

namespace yase {

    struct LockRequest {
        // Lock modes
        enum Mode {
            NL, // Not locked
            XL, // Exclusive lock
            SH, // Shared lock
        };

        // Requested mode
        uint64_t mode;

        // rid for the request
        RID rid;

        // The transaction that's requesting the lock
        Transaction *requester;

        // Whether the lock is granted to [requester]
        bool granted;

        LockRequest() : mode(Mode::NL), requester(nullptr), granted(false) {}
        LockRequest(Transaction *t) : mode(Mode::NL), requester(t), granted(false) {}
        ~LockRequest() {}
    };

    struct LockHead {
        // Current mode of the lock
        LockRequest::Mode current_mode;

        // Request queue
        std::list<LockRequest> requests;

        // Latch protecting the lock head (including the request queue)
        std::mutex latch;

        LockHead() : current_mode(LockRequest::Mode::NL) {}
        ~LockHead() {};
    };

    class LockManager {
    private:
        static LockManager *instance;

    public:
        inline static LockManager *Get() { return instance; }
        inline static void Initialize() {
            LockManager::instance = new LockManager;
        }

        LockManager() {}
        LockManager(LockManager const &) = delete;
        void operator=(LockManager const &) = delete;
        ~LockManager() {}

        // Request to lock a record
        // @tx: the transaction that is trying to lock a record
        // @rid: reference to the RID of the record to be locked
        // @mode: requested lock mode, must be XL or SH
        // Returns true if the lock is acquired, false otherwise
        bool AcquireLock(Transaction *tx, RID &rid, LockRequest::Mode mode);

        // Release a lock being held by the calling transaction
        // @tx: the transaction that is trying to release the lock on a record
        // @rid: reference to the RID of the locked record
        void ReleaseLock(Transaction *tx, RID &rid);

    private:
        // Lock table that maps RIDs (RID.value) to lock heads
        std::unordered_map<uint64_t, LockHead> lock_table;
        std::mutex latch_lock_table;
    };

}  // namespace yase
