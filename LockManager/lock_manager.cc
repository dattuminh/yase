/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 */
#include "lock_manager.h"

namespace yase {

// Timestamp counter
    std::atomic<uint64_t> Transaction::ts_counter(0);

// Static global lock manager instance
    LockManager *LockManager::instance = nullptr;

    bool LockManager::AcquireLock(Transaction *tx, RID &rid, LockRequest::Mode mode) {
        // 1. Take the latch that's protecting the lock table
        // 2. Look up the lock head for the given RID in the lock table:
        //    (a) If it exists in the lock table, release the lock table latch and
        //        enqueue (note: need to take the latch in lock head)
        //    (b) If it doesn't exist, create a new item in the lock table and the lock
        //        is granted. Set the proper fields in the lock head, release lock
        //        table latch and return true.
        // 3. Check lock compatibility to see if the lock can be granted by looking at
        //    the predecessor in the request queue.
        //    (a) If the predecessor has acquired the lock in a compatible mode, grant
        //        the lock.
        //    (b) Otherwise, use the *wait-die* algorithm to determine if the transaction
        //        given is allowed to wait, if not, remove tx from the queue.
        // 4. Release the lock head latch
        // 5. Add the lock request to the transactions list of lock requests if the
        //    lock is granted.
        // 6. Return true/false if the lock is granted/not granted.
        //
        // Note: you may find there are ways to optimize the above procedure, e.g., by
        // delaying the enqueue until you find the transaction is allowed to wait. You
        // are allowed to implement such optimizations; the above procedure is for your
        // reference to clarify the steps needed for this function.

        // Iterate through the lock table and take the latch
        // also check to see it the rid is present in the table
        latch_lock_table.lock();
        bool rid_present = !(lock_table.find(rid.value) == lock_table.end());

        // Create a new lock request
        auto *newLockRequest = new LockRequest(tx);
        newLockRequest->mode = mode;
        newLockRequest->rid = rid;

        // This is a brand new rid value that we are querying
        if (!rid_present) {
            newLockRequest->granted = true;

            // Create an entry in the lock table
            lock_table.emplace(std::piecewise_construct, std::make_tuple(rid.value), std::make_tuple());

            lock_table.at(rid.value).requests.emplace_back(*newLockRequest);
            lock_table.at(rid.value).current_mode = mode;
            latch_lock_table.unlock();

            // Add the lock request to the transaction lock request list
            tx->locks.emplace_back(newLockRequest);
            tx->state = Transaction::kStateInProgress;
            return true;
        }

        // The rid value exists in the lock table
        else {
            LockHead *lockHead = &lock_table.at(rid.value);

            // latch the lock head and release the latch for the table
            lockHead->latch.lock();
            latch_lock_table.unlock();

            // there are no lock requests in the lock head at rid.value
            if (lockHead->requests.empty()) {

                // insert the new request
                newLockRequest->granted = true;
                lockHead->requests.emplace_back(*newLockRequest);
                lockHead->latch.unlock();

                // insert the Transaction lock request
                tx->locks.emplace_back(newLockRequest);
                tx->state = Transaction::kStateInProgress;
                return true;
            }

            // Implement the wait-die algorithm
            else {

                // grab the predecessor
                LockRequest predecessor = lockHead->requests.back();

                // the predecessor is awaiting for another transaction
                if (!predecessor.granted) {
                    lockHead->latch.unlock();
                    return false;
                }

                // Grant the lock as NL / SH latches are compatible with each other
                else if (predecessor.mode == LockRequest::NL ||
                        (predecessor.mode == LockRequest::SH && mode == LockRequest::SH)) {
                    newLockRequest->granted = true;
                    lockHead->requests.emplace_back(*newLockRequest);

                    tx->locks.emplace_back(newLockRequest);
                    tx->state = Transaction::kStateInProgress;
                    return true;
                }

                // Locks aren't compatible. Need to check if we should wait or abort
                else {
                    // Get the transaction timestamps
                    uint64_t transaction_timestamp = tx->GetTimestamp();
                    uint64_t predecessor_timestamp = predecessor.requester->GetTimestamp();

                    // If the the transaction timestamp is smaller than that of the predecessor's
                    // then enqueue and wait for the lock to be granted
                    if (transaction_timestamp < predecessor_timestamp) {
                        lockHead->requests.emplace_back(*newLockRequest);
                        auto lockHeadPtr = &lockHead->requests.back();
                        lockHead->latch.unlock();

                        while (!lockHeadPtr->granted);
                        tx->locks.emplace_back(newLockRequest);
                        tx->state = Transaction::kStateInProgress;
                        return true;
                    }

                    // transaction timestamp is greater than that of the predecessor's
                    // do not enqueue
                    else {
                        lockHead->latch.unlock();
                        return false;
                    }
                }
            }
        }
    }

    void LockManager::ReleaseLock(Transaction *tx, RID &rid) {
        // 1. Latch the lock table and look up the given RID
        // 2. Unlatch the lock table, latch the lock head, traverse the lock queue to
        //    remove the node belonging to the given transaction
        // 3. Check and (if allowed) grant the next requester the lock
        // 4. Unlatch the lock head and remove the lock from list of locks in
        //    transaction

        // Lock the lock table
        latch_lock_table.lock();

        // Check if the lock_table actually has an entry with the rid. If not then unlock the latch and return
        if (lock_table.find(rid.value) == lock_table.end()) {
            latch_lock_table.unlock();
            return;
        }

        // grab the lock head, lock the lock head, and free the lock table latch
        auto lock_head = &lock_table.at(rid.value);
        lock_head->latch.lock();
        latch_lock_table.unlock();

        // Establish the lock head iterator and the current position to advance to in the request queue
        auto lh_iterator = lock_head->requests.begin();
        int pos = 0;

        // for loop to find the request to remove
        for (auto & request : lock_head->requests) {
            if (request.requester == tx) {

                // Unlatch the lock head so that we can remove it from the list
                lock_head->latch.unlock();

                // advance the iterator to the current position on the request queue
                advance(lh_iterator, pos);

                // erase the request
                lock_table.at(rid.value).requests.erase(lh_iterator);
                lock_head->latch.lock();
                break;
            }
            pos++;
        }

        if (!lock_head->requests.empty()) {
            LockRequest *request = &lock_head->requests.front();

            if (request->mode == LockRequest::XL) {
                lock_head->current_mode = LockRequest::XL;

                if (!request->granted) {
                    request->granted = true;
                }
            }

            else {
                lock_head->current_mode = LockRequest::SH;
                lock_head->requests.front().granted = true;
            }
        } else {
            lock_head->current_mode = LockRequest::NL;
        }

        lock_head->latch.unlock();

        // Remove the lock from the list of locks in that transaction
        auto tx_iterator = tx->locks.begin();
        pos = 0;

        for (auto & lock : tx->locks) {
            if (lock->rid.value == rid.value) {
                advance(tx_iterator, pos);

                tx->locks.erase(tx_iterator);
                break;
            }
            pos++;
        }
    }

    void Transaction::Commit() {
        // 1. Go over all the locks and release each of them
        // 2. Set transaction state to "committed"

        while (!locks.empty()) {
            auto & lock = locks.front();

            lock->mode = LockRequest::NL;
            lock->granted = false;
            locks.pop_front();
        }
        state = kStateCommitted;
    }

    uint64_t Transaction::Abort() {
        // 1. Iterate all the locks and release each of them
        // 2. Set transaction state to "aborted"
        // 3. Return the transaction's timestamp
        auto *lock_manager = LockManager::Get();

        while (!locks.empty()) {
            auto & lock = locks.front();
            lock_manager->ReleaseLock(lock->requester, lock->rid);
        }

        state = kStateAborted;
        return GetTimestamp();
    }

}  // namespace yase
