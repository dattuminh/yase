Contributions per member:

Milestone 1:
	
	- spa111:
		- PinPage function
		- UnpinPage function
		- AddFileMapping function
		
	- sma161:
		- Dual Table test
		- Extensive Debugging to find issues (Helped find issue with PinPage implementation)
 
	- ttu:
		- Buffer Manager allocation
		- Buffer Manager de-allocation

Milestone 2:
    - spa111:
        - SkipList implementation
        - PSkiplist implementation

    - sma161:
        - PSkiplist -> Table abstraction implementation
        - Test Cases

    -ttu:
        - Assisted spa111 on PSkiplist implementation

Milestone 3:
    - spa111:
        - Multi-threaded Data Access
        - Lock Manager

    - sma161:
        - Concurrent skiplist

    - ttu:
        - Lock Manager

Milestone 4:
    - spa111:
        - Benchmarking