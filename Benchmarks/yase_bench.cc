/*
 * YASE: Yet Another Storage Engine
 *
 * CMPT 454 Database Systems II (Summer 2019) Course Project
 * School of Computing Science, Simon Fraser University
 *
 * A simple index search benchmark
 *
 */
#include <iostream>
#include <thread>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <random>

#include "../yase_internal.h"
#include "../IndexManager/skiplist.h"
#include "../BufferManager/buffer_manager.h"
#include "../BufferManager/table.h"

// Use gflags to define command line arguments:
// threads
// seconds
// reads_per_round
// table_size - number of keys to load
// n_bpool_pages
DEFINE_uint64(threads, 8, "Number of concurrent worker threads");
DEFINE_uint64(n_bpool_pages, 150, "Number of buffer pool pages");
DEFINE_uint64(table_size, 10000, "Size of the table");
DEFINE_uint64(seconds, 60, "Duration of the experiment");
DEFINE_uint64(reads_per_round, 10000, "Number of random reads to do per round");

std::random_device randomDevice;  //Will be used to obtain a seed for the random number engine
std::mt19937 engine(randomDevice()); //Standard mersenne_twister_engine seeded with randomDevice()
std::uniform_int_distribution<uint64_t > distribution(0, FLAGS_table_size);

// One entry per thread to record the number of finished operations
std::vector<uint64_t> stats;

// Control whether each thread should start benchmarking
std::atomic<bool> bench_started(false);

// Control whether the entire benchmark is done
std::atomic<bool> shutdown(false);

void read_bench(uint64_t thread_id, yase::SkipList *slist) {
    // Wait for the start signal from main thread
    while (!bench_started) {}

    // Do the actual read requests
    while (!shutdown) {
        // 1. Use a random number generator to decide the key
        // 2. Issue a skip list search using slist
        // 3. Increment the corresponding stats variable (stats[thread_id]) after the
        //    read operation is done.
        // Note: there is no need to initialize a transaction and do commit. Plain
        // skip list Search operation is fine.

        for (long unsigned int i = 0; i < FLAGS_reads_per_round; i++) {
            uint64_t search_key = distribution(engine);
            yase::RID search_result = slist->Search((char *)&search_key);

            if (search_result.IsValid()) {
                stats[thread_id] = stats[thread_id] + 1;
            }
        }
    }
}

int main(int argc, char **argv) {
    // 1. Initialize glog and gflags.
    ::google::InitGoogleLogging(argv[0]);
    gflags::ParseCommandLineFlags(&argc, &argv, true);

    // 2. Initialize YASE buffer manager [FLAGS_n_bpool_pages] number of pages
    yase::BufferManager::Initialize(FLAGS_n_bpool_pages);

    // 3. Create a table with the following parameters
    static const uint32_t kFileId = 1;
    static const uint32_t kRecordSize = 8;
    static const uint32_t kDataSize = 128 * yase::MB;
    yase::Table table("test_file", kFileId, kRecordSize, kDataSize);

    // 4. Create an in-memory skiplist index that supports 8-byte keys
    yase::SkipList slist(kRecordSize);

    // 5. Load the database table with specified initial number of keys
    //    [FLAGS_table_size], and for each record, setup the key-RID mapping. The
    //    key and record values should both be the same as the record index. E.g.,
    //    the i-th record will have key = value = i. Do this using the loop below:
    for (uint64_t i = 0; i < FLAGS_table_size; ++i) {
        yase::RID rid(i);
        bool slist_ins_success = slist.Insert((char *)&i, rid);
        yase::RID table_ins = table.Insert((char *)&i);

        if (!slist_ins_success) {
            // Debug:
            std::cout << "Failed to slist insert key: " << i << "\n";
        }

        if (!table_ins.IsValid()) {
            // Debug:
            std::cout << "Failed to table insert key: " << i << "\n";
        }
    }

    // 6. Start the specified number of threads (FLAGS_threads), each of which runs
    //    the read_bench function.
    std::vector<std::thread *> threads;
    for (uint32_t i = 0; i < FLAGS_threads; ++i) {
        threads.push_back(new std::thread(read_bench, i, &slist));
    }

    // 7. Initialize the stats array with one 0 entry per thread.
    for (long unsigned int i = 0; i < FLAGS_threads; i++) {
        stats.push_back(0);
    }

    // 8. Flip bench_started to start benchmark
    bench_started = true;

    // 9. Sleep for one second and check whether the cumulative slept time is equal
    //    to [FLAGS_seconds]. If so, continue to the next stage.
    sleep(FLAGS_seconds);

    // 10. Set [shutdown] to true "tell" everyone to stop
    shutdown = true;

    // 11. Join and delete all threads
    for (auto &t : threads) {
        t->join();
        delete t;
    }
    threads.clear();

    // 12. Dump stats
    uint64_t nops = 0;
    for (uint32_t i = 0; i < stats.size(); ++i) {
        auto &s = stats[i];
        std::cout << "Thread " << i + 1 << ": " << s << " ops" << std::endl;
        nops += s;
    }

    std::cout << "Overall ops/s: " << (double)nops / FLAGS_seconds << std::endl;

    return 0;
}
